#!/bin/sh

# Clone repo if needed
cd 
if [[ ! -z $REPOSITORY ]] ; then 
    echo "Using $REPOSITORY"
    git clone "$REPOSITORY" config/
    # Copy XML to install path
    cp ~/config/${REPOSITORY_CONFIG_XML} /usr/share/CTP/config.xml

    #Create config path and copy config
    mkdir -p $CONFIG_PATH
    cp -r ~/config/${REPOSITORY_CONFIG} ${CONFIG_PATH}
fi

#  Start CTP
mkdir -p /usr/share/CTP/logs/ 
touch  /usr/share/CTP/logs/ctp.log

cd /usr/share/CTP/
java -jar /usr/share/CTP/Runner.jar &
tail -n 1024 -f /usr/share/CTP/logs/ctp.log
