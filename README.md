# ctp

CTP docker images

# Build local version

docker build -t local/ctp:latest .

# Usage either mount config and files in or set $REPOSITORY environment

export REPOSITORY=https://gitlab.com/radiology/infrastructure/docker-images/ctp.git
export REPO_CONFIG_XML=example/config.xml
export REPO_PATH=example/config
